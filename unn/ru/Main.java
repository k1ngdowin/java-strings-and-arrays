package unn.ru;


import java.util.Arrays;

public class Main {


    public static void main(String[] args) {
        double arr[] = {4, 7, 2, 9, 0, 1, 9, 12, 56, 78};
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] + (arr[i] / 100 * 10);
        }
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    double tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        String arrStr = Arrays.toString(arr);
        System.out.print(arrStr);
    }
}








